﻿using UnityEngine;
using System.Collections;

public class TuerCollider : MonoBehaviour {

    protected bool inFrontOfDoor = false;

    public string sceneToLoad = "Szene02";
    public float setPlayerToXPosition;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (inFrontOfDoor) {
                Bewegung.currentX = setPlayerToXPosition;
                Application.LoadLevel(sceneToLoad);
                
            }
        }
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        inFrontOfDoor = true;
    }

    void OnTriggerExit2D(Collider2D other)
    {
        inFrontOfDoor = false;
    }
}
