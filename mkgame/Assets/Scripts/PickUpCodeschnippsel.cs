﻿using UnityEngine;
using System.Collections;

public class PickUpCodeschnippsel : MonoBehaviour {
    public int punkte { get; set; }
    public int punktAnzahl { get; set; }
    private GameObject code;
    Rigidbody2D rb;
	// Use this for initialization
	void Start () 
    {
        rb = GetComponent<Rigidbody2D>();
        punktAnzahl = 1;
        code = GameObject.Find("Code01");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            Szene01.aktpunkte++;
          Debug.Log(Szene01.aktpunkte);
          

          Destroy(gameObject);
        
          
        }
    }
       
}
