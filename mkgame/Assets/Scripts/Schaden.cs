﻿using UnityEngine;
using System.Collections;

public class Schaden : MonoBehaviour {

    public GameObject spieler;
    protected bool hasTouched = false;

	// Use this for initialization
	void Start () {

        spieler = GameObject.FindWithTag("Player");
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Box-Links")
        {
            if (hasTouched == false) {
                hasTouched = true;
                Lebensanzeige.leben -= 1;
                if (Lebensanzeige.leben <= 0)
                {
                    Destroy(spieler);
                    Lebensanzeige.leben = 3;
                    Application.LoadLevel("main_menu");
                }
            }

        }
        else if (coll.gameObject.tag == "Box-Oben")
        {
            Destroy(coll.gameObject.transform.parent.gameObject);
        
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        hasTouched = false;
    }
}
