﻿using UnityEngine;
using System.Collections;

public class Szene01 : MonoBehaviour {

    public static int aktpunkte = 0;
    public static int maxpunkte = 3;

	// Use this for initialization
	void Start () {
        // den Spieler auf die Position setzen, die in Bewegung.currentX steht (kann von den Tueren veraendert werden)
        GameObject.Find("Player").transform.position = new Vector2(Bewegung.currentX, -0.4f);
	}
	
	// Update is called once per frame
	void Update () {
        checkKeyboardInput();
	}

    void checkKeyboardInput()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            Application.LoadLevel("main_menu");
        }
    }
}
