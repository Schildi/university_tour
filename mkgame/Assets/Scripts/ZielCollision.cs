﻿using UnityEngine;
using System.Collections;

public class ZielCollision : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            if (Szene01.aktpunkte == Szene01.maxpunkte) { 
            gewonnen();
            }
        }
    }

    void gewonnen()
    {
        // TODO: Zeit stoppen
        // TODO: Highscore speichern
        Lebensanzeige.leben = 3;
        Application.LoadLevel("main_menu");
    }
}
