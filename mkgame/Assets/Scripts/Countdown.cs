﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Countdown : MonoBehaviour {

    public Text txt;
    public float countdown = 30f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        Text text = txt.GetComponent<Text>();
        countdown -= Time.deltaTime;
        text.text = "Zeit: " + countdown.ToString("0");

        if(countdown <0)
        {
            Lebensanzeige.leben = 0;
            countdown = 30f;
        }
	}
}
