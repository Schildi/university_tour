﻿using UnityEngine;
using System.Collections;

public class Bewegung : MonoBehaviour {


   public float geschwindigkeit = 4f;
   public static float currentX;
   bool rechts = true;
   Rigidbody2D rb;

   void Start()
   {
       rb = GetComponent<Rigidbody2D>();
       transform.eulerAngles = new Vector2(0, 180);
   }

    void FixedUpdate()
    {
        float move = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(move * geschwindigkeit, rb.velocity.y);

        if (Input.GetKey(KeyCode.LeftArrow)) 
        {
            
            FlipRechts();
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            Flip();
        }
    }

    void Flip()
    {
        rechts = !rechts;
        transform.eulerAngles = new Vector2(0, 180);
    }

    void FlipRechts()
    {
        rechts = !rechts;
        transform.eulerAngles = new Vector2(0, 0);
    }


}
