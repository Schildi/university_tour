﻿using UnityEngine;
using System.Collections;

public class Feindeeinfach : MonoBehaviour
{

    public float speed1 = 10f;
   
    static Rigidbody2D rb1;
    public GameObject Boden;
    // Use this for initialization
    void Start()
    {
        rb1 = GetComponent<Rigidbody2D>();
    }

    public bool Feinde = false;

    void OnCollisionEnter2D(Collision2D coll)
    {
       
        if (coll.gameObject.tag == "Boden")
        {

            Feinde = true;
        }
        if (coll.gameObject.tag == "Player")
        {

            Feinde = true;
        }

    }
    // Update is called once per frame

    void Update()
    {
        if (Feinde == false)
        {
            Vector3 pos = transform.position;
            pos.x += speed1 * Time.deltaTime;
            transform.position = pos;
        }
        if (Feinde == true)
        {
 
            speed1 *= -1;
           
        

            Feinde = false;
        
        }


    }
}

