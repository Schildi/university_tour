﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Lebensanzeige : MonoBehaviour {


    public int maxhealth = 3;
    static public int leben = 3;
    public Text txt;
    public GameObject feind;
    public GameObject spieler; 

    void Start(){

        spieler = GameObject.FindWithTag("Player");
        feind = GameObject.Find("Feind");
    }


    void Update ()
    {

        Text other = txt.GetComponent<Text>();
        other.text = "Leben: " + leben;
        if (leben <= 0)
        {
            Destroy(spieler);
            Application.LoadLevel("main_menu");
        }


    }

    void Schaden ()
    {
    }

}
