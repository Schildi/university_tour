﻿using UnityEngine;
using System.Collections;

public class MainMenuScript : MonoBehaviour {

    private bool isFirstMenu = true;
    private bool isLoadMenu = false;
    private bool isLevelSelectMenue = false;
    private bool isOptionMenu = false;

    public string gameTitle = "mkgame";
    public Font font;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnGUI()
    {
        GUIStyle style = new GUIStyle();
        style.fontSize = 48;
        style.font = font;

        GUI.Label(new Rect(Screen.width / 2 - 75, Screen.height / 2 - 175, 300, 25), gameTitle, style);

       
        FirstMenu();
        LoadGameMenu();
        LevelSelectMenue();
        OptionsMenu();

        if (isLoadMenu == true || isLevelSelectMenue == true || isOptionMenu == true)
        {
            if (GUI.Button(new Rect(Screen.width - 75, Screen.height - 35, 150, 25), "Zurück"))
            {
                isLoadMenu = false;
                isLevelSelectMenue = false;
                isOptionMenu = false;
                isFirstMenu = true;
            }
        }
    }

    void FirstMenu()
    {
        if (isFirstMenu)
        {

            if (GUI.Button(new Rect(Screen.width / 2 - 75, Screen.height / 2 - 100, 150, 25), "Neues Spiel"))

            {
                Debug.Log("Hello");
                Application.LoadLevel("Szene01");
            }

            if (GUI.Button(new Rect(Screen.width / 2 - 75, Screen.height / 2 - 65, 150, 25), "Fortsetzen"))
            {
                isFirstMenu = false;
                isLoadMenu = true;
            }

            if (GUI.Button(new Rect(Screen.width / 2 - 75, Screen.height / 2 - 30, 150, 25), "Levelauswahl"))
            {
                isFirstMenu = false;
                isLevelSelectMenue = true;
            }

            if (GUI.Button(new Rect(Screen.width / 2 - 75, Screen.height / 2 + 5, 150, 25), "Credits"))
            {
                isFirstMenu = false;
                isOptionMenu = true;
            }

            if (GUI.Button(new Rect(Screen.width / 2 - 75, Screen.height / 2 + 40, 150, 25), "Ende"))
            {
                Application.Quit();
            }
        }
    }

    void LoadGameMenu()
    {
        if (isLoadMenu)
        {

        }
    }

    void LevelSelectMenue()
    {
        if (isLevelSelectMenue)
        {

        }
    }

    void OptionsMenu()
    {
        if (isOptionMenu)
        {

                if (GUI.Button(new Rect(Screen.width - 75, Screen.height - 35, 150, 25), "Zurück"))
                {
                    GUI.Label(new Rect(Screen.width / 2, Screen.height / 2, 300, 50), "awdawd");
                    isLoadMenu = false;
                    isLevelSelectMenue = false;
                    isOptionMenu = false;
                    isFirstMenu = true;
                }
            

        }
    }
}
